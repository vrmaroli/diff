/*
 * UNIX diff command using threads
 * author: Vishnu Ramesh Maroli
 */

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <string>
#include <queue>

using namespace std;

// struct definition
typedef struct {
    istream& stream;
    queue<string>& buffer;
    bool& finished;
} threadInfoStruct;

// global variables
bool finishedReadingFile1;
bool finishedReadingFile2;
queue<string> bufferFile1;
queue<string> bufferFile2;

// function defs
void* readFromStream(void *args);
void* diffAndWriteToStream(void *args);

int main(int argc, char *argv[]) {
	// arguement handling
	if(argc > 2);
	else {
		cerr << "usage: diff [filename1] [filename2]" << endl ;
		return 1;
	}

	finishedReadingFile1 = false;
	finishedReadingFile2 = false;
	ifstream in1 (argv[1], ifstream::in);
	ifstream in2 (argv[2], ifstream::in);

	pthread_t threadForFile1;
	threadInfoStruct *forFile1 = new threadInfoStruct{in1, bufferFile1, finishedReadingFile1};
	pthread_create(&threadForFile1, NULL, &readFromStream, (void*)forFile1);

	pthread_t threadForFile2;
	threadInfoStruct *forFile2 = new threadInfoStruct{in2, bufferFile2, finishedReadingFile2};
	pthread_create(&threadForFile2, NULL, &readFromStream, (void*)forFile2);

	pthread_t threadForDiffAndOut;
	pthread_create(&threadForDiffAndOut, NULL, &diffAndWriteToStream, (void*)&cout);

	pthread_join(threadForFile1, NULL);
	pthread_join(threadForFile2, NULL);
	pthread_join(threadForDiffAndOut, NULL);

	in1.close();
	in2.close();
    return 0;
}

void* readFromStream(void *args) {
	threadInfoStruct *t_info = (threadInfoStruct*)args;
	string in;
	while(!t_info->stream.eof()) {
		getline(t_info->stream, in);
		t_info->buffer.push(in);
	}
	t_info->finished = true;
	pthread_exit(0);
}

void* diffAndWriteToStream(void *args) {
	ostream *stream = (ostream*)args;
	while(true) {
		if(finishedReadingFile1 and finishedReadingFile2) {
			while(!bufferFile1.empty() and !bufferFile2.empty()) {
				// finished reading both files
				if(bufferFile1.front().compare(bufferFile2.front())==0);	//equal
				else {
					*stream << bufferFile1.front() << endl;
					*stream << bufferFile2.front() << endl;
				}
				bufferFile1.pop();
				bufferFile2.pop();
			}
			while(!bufferFile1.empty()) {
				*stream << bufferFile1.front() << endl;
				bufferFile1.pop();
			}
			while(!bufferFile2.empty()) {
				*stream << bufferFile2.front() << endl;
				bufferFile2.pop();
			}
			break;
		}
		else if(finishedReadingFile1 and !finishedReadingFile2) {
			// finished reading file1
			while(!bufferFile1.empty() and !bufferFile2.empty()) {
				if(bufferFile1.front().compare(bufferFile2.front())==0);	//equal
				else {
					*stream << bufferFile1.front() << endl;
					*stream << bufferFile2.front() << endl;
				}
				bufferFile1.pop();
				bufferFile2.pop();
			}
			if(bufferFile1.empty()) while(!bufferFile2.empty()) {
				*stream << bufferFile2.front() << endl;
				bufferFile2.pop();
			}
		}
		else if(!finishedReadingFile1 and finishedReadingFile2) {
			// finished reading file 2
			while(!bufferFile1.empty() and !bufferFile2.empty()) {
				if(bufferFile1.front().compare(bufferFile2.front())==0);	//equal
				else {
					*stream << bufferFile1.front() << endl;
					*stream << bufferFile2.front() << endl;
				}
				bufferFile1.pop();
				bufferFile2.pop();
			}
			if(bufferFile2.empty()) while(!bufferFile1.empty()) {
				*stream << bufferFile1.front() << endl;
				bufferFile1.pop();
			}
		}
		else {
			// havent finished reading
			while(!bufferFile1.empty() and !bufferFile2.empty()) {
				if(bufferFile1.front().compare(bufferFile2.front())==0);	//equal
				else {
					*stream << bufferFile1.front() << endl;
					*stream << bufferFile2.front() << endl;
				}
				bufferFile1.pop();
				bufferFile2.pop();
			}
		}
	}
	pthread_exit(0);
}